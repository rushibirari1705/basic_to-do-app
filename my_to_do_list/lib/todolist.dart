import "package:flutter/material.dart";
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

class MyToDoList extends StatefulWidget {
  const MyToDoList({super.key});

  @override
  State<MyToDoList> createState() => _MyToDoListState();
}

class ToDoModelClass {
  final String title;
  final String description;
  final String date;

  ToDoModelClass({
    required this.title,
    required this.description,
    required this.date,
  });
}

class _MyToDoListState extends State<MyToDoList> {

  TextEditingController titleController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController datePickerController = TextEditingController();

  bool titleValidate = false;
  bool desValidate = false;
  bool dateValidate = false;


  // ignore: non_constant_identifier_names
  List card_list = [ 
    ToDoModelClass(title: "Buy Grocerey from DMart", description: "cooking , maggie n all", date: "Feb 28,2024"),
    ToDoModelClass(title: "Exam form submission", description: "due date is coming", date: "Mar 1,2024"),

  ];
  // ignore: non_constant_identifier_names
  List color_list = const [
    Color.fromRGBO(250, 232, 220, 1),
    Color.fromRGBO(232, 237, 250, 1),
    Color.fromRGBO(250, 249, 232, 1),
    Color.fromRGBO(250, 232, 250, 1)
  ];


  void editBottomsheet(ToDoModelClass obj,int  index) {

    titleController.text = obj.title;
    descriptionController.text = obj.description;
    datePickerController.text = obj.date;

    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (BuildContext context) {
          return Padding(
            padding: MediaQuery.of(context).viewInsets,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Center(
                  child: Padding(
                    padding: EdgeInsets.only(top: 8.0),
                    child: Text(" Create Task ",
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 22,
                          color: Colors.black,
                        )),
                  ),
                ),
                const SizedBox(height: 10),
                Container(
                  margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
                  child: const Text(
                    " Title ",
                    style: TextStyle(
                      fontSize: 11,
                      color: Color.fromRGBO(0, 139, 148, 1),
                    ),
                  ),
                ),
                Container(
                  height: 50,
                  width: double.infinity,
                  alignment: Alignment.center,
                  margin: const EdgeInsets.fromLTRB(10, 2, 15, 0),
                  child: TextField(
                    controller: titleController,
                    decoration: InputDecoration(
                        hintText: "Enter task",
                        errorText:
                            titleValidate ? 'Value Cannot Be Empty' : null,
                        enabledBorder: const OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Color.fromRGBO(0, 139, 148, 1),
                            width: 2.0,
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                        )),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Container(
                  margin: const EdgeInsets.fromLTRB(10, 0, 0, 10),
                  child: const Text(
                    " Description ",
                    style: TextStyle(
                      fontSize: 11,
                      color: Color.fromRGBO(0, 139, 148, 1),
                    ),
                  ),
                ),
                Container(
                  height: 80,
                  width: double.infinity,
                  margin: const EdgeInsets.fromLTRB(10, 2, 15, 0),
                  child: TextField(
                    controller: descriptionController,
                    expands: true,
                    maxLines: null,
                    decoration: InputDecoration(
                        hintText: " Enter Details ",
                        errorText: desValidate ? 'Value Cannot Be Empty' : null,
                        enabledBorder: const OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Color.fromRGBO(0, 139, 148, 1),
                            width: 2.0,
                          ),
                        )),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Container(
                  margin: const EdgeInsets.fromLTRB(10, 0, 0, 10),
                  child: const Text(
                    " Date ",
                    style: TextStyle(
                      fontSize: 11,
                      color: Color.fromRGBO(0, 139, 148, 1),
                    ),
                  ),
                ),
                Container(
                  height: 50,
                  width: double.infinity,
                  margin: const EdgeInsets.fromLTRB(10, 2, 15, 0),
                  child: TextField(
                    controller: datePickerController,
                    decoration: InputDecoration(
                        errorText:
                            dateValidate ? 'Value Cannot Be Empty' : null,
                        suffixIcon: const Icon(Icons.calendar_month_rounded),
                        hintText: "Select Date"),
                    readOnly: true,
                    onTap: () async {
                      DateTime? pickedDate = await showDatePicker(
                          context: context,
                          initialDate: DateTime.now(),
                          firstDate: DateTime(2023),
                          lastDate: DateTime(2100));

                      String formatedDate =
                          DateFormat.yMMMd().format(pickedDate!);

                      setState(() {
                        datePickerController.text = formatedDate;
                      });
                    },
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Center(
                  child: SizedBox(
                    height: 50,
                    width: 300,
                    child: Padding(
                      padding: const EdgeInsets.only(bottom: 10.0),
                      child: ElevatedButton(
                          onPressed: () {
                            setState(() {
                              String enteredTitle = titleController.text.trim();
                              String enteredDescription =
                                  descriptionController.text.trim();
                              String enteredDate =
                                  datePickerController.text.trim();

                              enteredTitle.isEmpty
                                  ? titleValidate = true
                                  : titleValidate = false;
                              enteredDescription.isEmpty
                                  ? desValidate = true
                                  : desValidate = false;
                              enteredDate.isEmpty
                                  ? dateValidate = true
                                  : dateValidate = false;

                              if (dateValidate == false && titleValidate == false && desValidate == false) {
                                  card_list[index] = ToDoModelClass(
                                  title: titleController.text, 
                                  description: descriptionController.text,
                                  date: datePickerController.text,
                                );
                                
                              }
                              

                            });

                            Navigator.of(context).pop();
                            titleController.clear();
                            descriptionController.clear();
                            datePickerController.clear();

                            titleValidate = false;
                            desValidate = false;
                            dateValidate = false;

                          },
                          style: ElevatedButton.styleFrom(
                            backgroundColor:
                                const Color.fromRGBO(0, 139, 148, 1),
                            foregroundColor: Colors.white,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                          child: const Text(
                            " Update ",
                            style: TextStyle(
                              fontSize: 20,
                            ),
                          )),
                    ),
                  ),
                )
              ],
            ),
          );
        });
  }

// After submit button click
  Future addBottomsheet() {

    return showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (BuildContext context) {
          return Padding(
            padding: MediaQuery.of(context).viewInsets,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Center(
                  child: Padding(
                    padding: EdgeInsets.only(top: 8.0),
                    child: Text(" Create Task ",
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 22,
                          color: Colors.black,
                        )),
                  ),
                ),
                const SizedBox(height: 10),
                Container(
                  margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
                  child: const Text(
                    " Title ",
                    style: TextStyle(
                      fontSize: 11,
                      color: Color.fromRGBO(0, 139, 148, 1),
                    ),
                  ),
                ),
                Container(
                  height: 50,
                  width: double.infinity,
                  alignment: Alignment.center,
                  margin: const EdgeInsets.fromLTRB(10, 2, 15, 0),
                  child: TextField(
                    controller: titleController,
                    decoration: InputDecoration(
                        hintText: "Enter task",
                        errorText:
                            titleValidate ? 'Value Cannot Be Empty' : null,
                        enabledBorder: const OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Color.fromRGBO(0, 139, 148, 1),
                            width: 2.0,
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                        )),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Container(
                  margin: const EdgeInsets.fromLTRB(10, 0, 0, 10),
                  child: const Text(
                    " Description ",
                    style: TextStyle(
                      fontSize: 11,
                      color: Color.fromRGBO(0, 139, 148, 1),
                    ),
                  ),
                ),
                Container(
                  height: 80,
                  width: double.infinity,
                  margin: const EdgeInsets.fromLTRB(10, 2, 15, 0),
                  child: TextField(
                    controller: descriptionController,
                    expands: true,
                    maxLines: null,
                    decoration: InputDecoration(
                        hintText: " Enter Details ",
                        errorText: desValidate ? 'Value Cannot Be Empty' : null,
                        enabledBorder: const OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Color.fromRGBO(0, 139, 148, 1),
                            width: 2.0,
                          ),
                        )),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Container(
                  margin: const EdgeInsets.fromLTRB(10, 0, 0, 10),
                  child: const Text(
                    " Date ",
                    style: TextStyle(
                      fontSize: 11,
                      color: Color.fromRGBO(0, 139, 148, 1),
                    ),
                  ),
                ),
                Container(
                  height: 50,
                  width: double.infinity,
                  margin: const EdgeInsets.fromLTRB(10, 2, 15, 0),
                  child: TextField(
                    controller: datePickerController,
                    decoration: InputDecoration(
                        errorText:
                            dateValidate ? 'Value Cannot Be Empty' : null,
                        suffixIcon: const Icon(Icons.calendar_month_rounded),
                        hintText: "Select Date"),
                    readOnly: true,
                    onTap: () async {
                      DateTime? pickedDate = await showDatePicker(
                          context: context,
                          initialDate: DateTime.now(),
                          firstDate: DateTime(2023),
                          lastDate: DateTime(2100));

                      String formatedDate =
                          DateFormat.yMMMd().format(pickedDate!);

                      setState(() {
                        datePickerController.text = formatedDate;
                      });
                    },
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Center(
                  child: SizedBox(
                    height: 50,
                    width: 300,
                    child: Padding(
                      padding: const EdgeInsets.only(bottom: 10.0),
                      child: ElevatedButton(
                          onPressed: () {
                            setState(() {
                              String enteredTitle = titleController.text.trim();
                              String enteredDescription =
                                  descriptionController.text.trim();
                              String enteredDate =
                                  datePickerController.text.trim();

                              enteredTitle.isEmpty
                                  ? titleValidate = true
                                  : titleValidate = false;
                              enteredDescription.isEmpty
                                  ? desValidate = true
                                  : desValidate = false;
                              enteredDate.isEmpty
                                  ? dateValidate = true
                                  : dateValidate = false;

                              if (dateValidate == false && titleValidate == false && desValidate == false) {
                                card_list.add(ToDoModelClass(
                                  title: titleController.text,
                                  description: descriptionController.text,
                                  date: datePickerController.text,
                                ));

                                titleController.text = "";
                                descriptionController.text = '';
                                datePickerController.text = '';
                                

                                titleValidate = false;
                                desValidate = false;
                                dateValidate = false;
                              }else{
                                Navigator.pop(context);
                              }
                            });
                          },
                          style: ElevatedButton.styleFrom(
                            backgroundColor:
                                const Color.fromRGBO(0, 139, 148, 1),
                            foregroundColor: Colors.white,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                          child: const Text(
                            " Submit ",
                            style: TextStyle(
                              fontSize: 20,
                            ),
                          )),
                    ),
                  ),
                )
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {},
          icon: const Icon(
            Icons.menu,
            size: 30,
            color: Colors.white,
            weight: 50.0,
          ),
        ),
        elevation: 0,
        title: Text(
          " To-do App",
          style: GoogleFonts.quicksand(
              textStyle: const TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w700,
            fontSize: 26,
          )),
        ),
        backgroundColor: const Color.fromRGBO(0, 139, 148, 1),
      ),
      body: ListView.builder(
        itemCount: card_list.length,
        itemBuilder: ((context, index) {
          return Padding(
            padding: const EdgeInsets.only(top: 15.0, bottom: 8.0),
            child: Container(
                height: 135,
                width: 310,
                margin: const EdgeInsets.symmetric(horizontal: 15.5),
                decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(Radius.circular(15)),
                  color: color_list[index % color_list.length],
                  boxShadow: const [
                    BoxShadow(
                      color: Color.fromRGBO(0, 0, 0, 0.1),
                      offset: Offset(0, 8),
                      blurRadius: 8,
                      spreadRadius: 1,
                    ),
                  ],
                ),
                child: Column(
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(
                            top: 8.0,
                            left: 100,
                          ),
                          child: Expanded(
                            child: SizedBox(
                                height: 23,
                                width: 250,
                                child: Text(
                                  card_list[index].title,
                                  style: GoogleFonts.quicksand(
                                      textStyle: const TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w600,
                                    fontSize: 16,
                                  )),
                                )),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 2,
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(16, 0, 0, 0),
                      child: Row(
                        children: [
                          Container(
                            height: 52,
                            width: 52,
                            decoration: const BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(30)),
                              color: Color.fromARGB(255, 255, 255, 255),
                              boxShadow: [
                                BoxShadow(
                                  color: Color.fromRGBO(0, 0, 0, 0.07),
                                  offset: Offset(0, 0),
                                  blurRadius: 8,
                                  spreadRadius: 0,
                                ),
                              ],
                            ),
                            child: Image.asset(
                              "images/v1.jpg",
                              height: 21,
                              width: 25,
                            ),
                          ),
                          const SizedBox(
                            width: 25,
                          ),
                          SizedBox(
                            height: 50,
                            width: 250,
                            child: Expanded(
                              child: Text(
                                card_list[index].description,
                                style: GoogleFonts.quicksand(
                                  textStyle: const TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 14,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.fromLTRB(10, 0, 0, 5),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              SizedBox(
                                height: 15,
                                width: 100,
                                child: Text(
                                  card_list[index].date,
                                  style: GoogleFonts.quicksand(
                                    textStyle: const TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 14,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        const Spacer(),
                        Padding(
                          padding: const EdgeInsets.only(right: 0.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Column(
                                children: [
                                  IconButton(
                                      onPressed: (){
                                        editBottomsheet(card_list[index],index);
                                      },
                                      iconSize: 17,
                                      color:
                                          const Color.fromRGBO(0, 139, 148, 1),
                                      icon: const Icon(Icons.edit)),
                                ],
                              ),
                              Column(
                                children: [
                                  IconButton(
                                      onPressed: () {
                                        setState(() {
                                          card_list.removeAt(index);
                                        });
                                      },
                                      iconSize: 17,
                                      color:
                                          const Color.fromRGBO(0, 139, 148, 1),
                                      icon: const Icon(Icons.delete_rounded)),
                                ],
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                )),
          );
        }),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: addBottomsheet,
        child: const Icon(
          Icons.add_task_outlined,
          size: 40,
          color: Color.fromRGBO(0, 139, 148, 1),
        ),
      ),
    );
  }
}
